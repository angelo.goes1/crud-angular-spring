package com.angelogoes.crudspring.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.angelogoes.crudspring.model.Course;
import com.angelogoes.crudspring.repository.CourseRepository;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/courses")
@AllArgsConstructor
public class CourseController {

    private final CourseRepository courseRepository;

    @GetMapping
    public List<Course> list() {
        return courseRepository.findAll();
    }

    @GetMapping("/{id}")
    public Course getById(@PathVariable("id") Long id) {
        Optional<Course> course = courseRepository.findById(id);
        return course.get();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Course create(@RequestBody Course course) {
        return courseRepository.save(course);
        //return ResponseEntity.status(HttpStatus.CREATED)
        //        .body(courseRepository.save(course));

    }

    @PutMapping
    public Course update(@RequestBody Course course) {
        return create(course);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        courseRepository.deleteById(id);
    }

}
