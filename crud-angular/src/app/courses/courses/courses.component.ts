import { ConfirmDeletionDialogComponent } from './../../shared/components/confirm-deletion-dialog/confirm-deletion-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { catchError, Observable, tap, of, Subscription } from 'rxjs';
import { ErrorDialogComponent } from 'src/app/shared/components/error-dialog/error-dialog.component';

import { Course } from './../models/course';
import { CoursesService } from './../services/courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
})
export class CoursesComponent implements OnInit, OnDestroy {
  listSub!: Subscription;
  courses$!: Observable<Course[]>;

  displayedColumns = ['name', 'category', 'actions'];

  constructor(
    private service: CoursesService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnDestroy(): void {
    //this.service.listeningEmitter().unsubscribe();
    this.listSub.unsubscribe();
  }

  ngOnInit(): void {
    this.listCourses();
    this.listeningEvent();
  }

  listeningEvent() {
    this.listSub = this.service.listeningEmitter().subscribe(() => {
      this.listCourses();
    });
  }

  listCourses() {
    //this.service.reloadComponent('/courses');
    this.courses$ = this.service.list().pipe(
      //tap((n) => console.log(n)),
      catchError(() => {
        this.onError('Erro ao carregar cursos.');
        return of([]);
      })
    );
  }

  onError(errorMsg: string) {
    this.dialog.open(ErrorDialogComponent, {
      data: errorMsg,
    });
  }

  onAdd() {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  onEdit(course: Course) {
    let url = `edit/${course._id}`;
    this.router.navigate([url], { relativeTo: this.route });
  }

  onDelete(course: Course) {
    this.dialog
      .open(ConfirmDeletionDialogComponent, { data: course.name })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.service.delete(Number(course._id)).subscribe(
            (_result) => {
              this.service.onSuccess('Curso deletado com sucesso.'),
                this.listCourses();
            },
            (_error) => this.service.onError('Erro ao deletar curso.')
          );
        }
      });
  }
}
