import { IFormCanDeactivate } from './../guards/iform-candeactivate.guard';
import { CoursesService } from './../services/courses.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { tap } from 'rxjs';
//import { Location } from '@angular/common';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss'],
})
export class CourseFormComponent implements OnInit, IFormCanDeactivate {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: CoursesService //private location: Location
  ) {
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(service.minNameLenght),
          Validators.maxLength(service.maxNameLenght),
        ],
      ],
      category: [null],
    });
  }

  ngOnInit(): void {}

  onSubmit() {
    // set as pristine first so it doesn't activate the deactivate guard
    if (this.form.valid) {
      this.form.markAsPristine();

      this.service
        .save(this.form.value)
        .pipe(
          tap(() => {
            this.service.emitter();
          })
        )
        .subscribe(
          (_result) => this.service.onSuccess('Curso salvo com sucesso.'),
          (_error) => this.service.onError('Erro ao criar curso.')
        );
      this.onCancel();
    }
  }

  onCancel() {
    // set as pristine first so it doesn't activate the deactivate guard
    this.form.markAsPristine();
    this.service.onCancel();
  }

  routeDeactivate() {
    if (this.form.dirty)
      return confirm(
        'Deseja sair da página? Todas as modificações serão apagadas.'
      );
    return true;
  }
}
