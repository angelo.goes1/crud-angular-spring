//import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IFormCanDeactivate } from '../guards/iform-candeactivate.guard';

import { Course } from '../models/course';
import { CoursesService } from './../services/courses.service';

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.scss'],
})
export class CourseEditComponent implements OnInit, IFormCanDeactivate {
  form: FormGroup;
  course!: Course;

  constructor(
    private formBuilder: FormBuilder,
    private service: CoursesService,
    //private location: Location,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(service.minNameLenght),
          Validators.maxLength(service.maxNameLenght),
        ],
      ],
      category: [null],
    });
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id')!;
    this.service.readById(id).subscribe(
      (course) => {
        this.course = course;
        this.form.setValue({
          name: this.course.name,
          category: this.course.category,
        });
      },
      (_error) => {
        this.service.onError('Curso não encontrado.'), this.onCancel();
      }
    );
  }

  onSubmit() {
    if (this.form.valid) {
      // set as pristine first so it doesn't activate the deactivate guard
      this.form.markAsPristine();

      this.course.name = this.form.value['name'];
      this.course.category = this.form.value['category'];

      this.service.update(this.course).subscribe(
        (_result) => this.service.onSuccess('Curso editado com sucesso.'),
        (_error) => this.service.onError('Erro ao editar curso.')
      );

      this.onCancel();
    }
  }

  onCancel() {
    // set as pristine first so it doesn't activate the deactivate guard
    this.form.markAsPristine();
    this.service.onCancel();
    //this.location.back();
  }

  routeDeactivate() {
    if (this.form.dirty)
      return confirm(
        'Deseja parar de editar? Todas as modificações serão apagadas.'
      );
    return true;
  }
}
