import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CourseEditComponent } from './course-edit/course-edit.component';
import { CourseFormComponent } from './course-form/course-form.component';
import { CoursesComponent } from './courses/courses.component';
import { CoursesCanDeactivateGuard } from './guards/courses-candeactivate.guard';


//import { NotFoundComponent } from './not-found/not-found.component';
const routes: Routes = [
  { path: 'new', component: CourseFormComponent, canDeactivate: [CoursesCanDeactivateGuard]},
  { path: 'edit/:id', pathMatch: 'full', component: CourseEditComponent, canDeactivate: [CoursesCanDeactivateGuard]},
  { path: '', component: CoursesComponent},
  { path: '**', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
