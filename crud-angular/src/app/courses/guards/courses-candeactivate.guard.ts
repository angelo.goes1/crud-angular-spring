import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { CourseFormComponent } from '../course-form/course-form.component';
import { IFormCanDeactivate } from './iform-candeactivate.guard';

@Injectable()
export class CoursesCanDeactivateGuard
  implements CanDeactivate<IFormCanDeactivate>
{
  canDeactivate(
    component: IFormCanDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    //console.log('desativou');
    //return confirm('Deseja sair da página? Todas as modificações serão apagadas.');
    
    return component.routeDeactivate();
  }
}

