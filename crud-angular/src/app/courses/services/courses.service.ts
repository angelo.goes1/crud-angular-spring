import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { delay, first, Observable } from 'rxjs';

import { Course } from './../models/course';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  readonly minNameLenght: number = 1;
  readonly maxNameLenght: number = 50;
  eventEmitter: EventEmitter<any> = new EventEmitter();

  private readonly API: string = 'api/courses';

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  emitter() {
    return this.eventEmitter.emit();
  }

  listeningEmitter() {
    return this.eventEmitter;
  }

  list() {
    return this.httpClient.get<Course[]>(this.API).pipe(delay(200), first());
  }

  readById(id: number): Observable<Course> {
    const url = `${this.API}/${id}`;
    return this.httpClient.get<Course>(url).pipe(first());
  }

  save(record: Course) {
    return this.httpClient.post<Course>(this.API, record).pipe(first());
  }

  delete(id: number) {
    const url = `${this.API}/${id}`;
    return this.httpClient.delete<Course>(url).pipe(first());
  }

  update(record: Course) {
    return this.httpClient.put<Course>(this.API, record).pipe(first());
  }

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success'],
    });
  }

  onError(msg: string) {
    this.showMessage(msg, true);
  }

  onSuccess(msg: string) {
    this.showMessage(msg, false);
  }

  onCancel() {
    this.router.navigate(['/courses']);
  }

  // reload page by navigating to same route strategy
  /*
  reloadComponent(route: string) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([route]);
  }*/
}
